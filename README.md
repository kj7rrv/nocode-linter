# nocode-linter

Linter for No Code: https://github.com/kelseyhightower/nocode

## Usage

    python3 nolint.py <file>
