#!/usr/bin/env python3
import argparse

parser = argparse.ArgumentParser(description="Linter for No Code.")
parser.add_argument("file", help="name of No Code file to lint")
args = parser.parse_args()

try:
    with open(args.file) as f:
        content = f.read()
    if content:
        if content.strip():
            if args.file.endswith(".no"):
                print("Not very good.")
                print(" * File has non-whitespace content")
                print("At least there's something good, though:")
                print(" * File extension is `.no`")
                print("Suggestions:")
                print(" * Delete the file")
            else:
                print("Not good. At all.")
                print(" * File has non-whitespace content")
                print(" * File extension is not `.no`")
                print("Suggestions:")
                print(" * Delete the file")
                print(" * Use `.no` file extension")
        else:
            if args.file.endswith(".no"):
                print("Not terrible...")
                print(" * All content is whitespace")
                print(" * File extension is `.no`")
                print("There's room for improvement, however:")
                print(" * Delete the file")
            else:
                print("Okay...")
                print(" * All content is whitespace")
                print("There's room for improvement, however:")
                print(" * Delete the file")
                print(" * Use `.no` file extension")
    else:
        if args.file.endswith(".no"):
            print("Good job!")
            print(" * File is empty")
            print(" * File extension is `.no`")
            print("There's room for improvement, however:")
            print(" * Delete the file")
        else:
            print("Pretty good!")
            print(" * File is empty")
            print("There's room for improvement, however:")
            print(" * Delete the file")
            print(" * Use `.no` file extension")
except FileNotFoundError:
    if args.file.endswith(".no"):
        print("Excellent!")
        print(" * File extension is `.no`")
        print(" * File does not exist")
    else:
        print("Great!")
        print(" * File does not exist")
        print("There's room for improvement, however:")
        print(" * Use `.no` file extension")
